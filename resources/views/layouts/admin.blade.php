<!doctype html>

<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Admin</title>

		<link rel="stylesheet" href="{{ mix('/css/app.css') }}">
		<script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
	</head>

	<body>
		@include('partials.admintopnav')
    
    @include('partials.adminsidenav')

		@yield('content')
	</body>
</html>