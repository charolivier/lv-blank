@extends('layouts.admin')

@section('content')
  <section class="main">
    <div class="row">
      <div class="col-md-12">
        <h1>Edit Posts</h1>

        <hr>

        @include('partials.errors')

        <form action="{{ route('admin.update') }}" method="post">
          <div class="form-group">
            <label for="title">Title</label>

            <input type="text" class="form-control" id="title" name="title" value="{{ $post->title }}">
          </div>

          <div class="form-group">
            <label for="content">Content</label>

            <textarea class="form-control" id="content" name="content">{{ $post->content }}</textarea>
          </div>

          {{ csrf_field() }}

          <input type="hidden" name="id" value="{{ $postId }}">

          <button type="submit" class="btn btn-primary">
            <span>Submit</span>
          </button>
        </form>
      </div>
    </div>
  </section>
@endsection