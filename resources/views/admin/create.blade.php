@extends('layouts.admin')

@section('content')
  <section class="main">
    <div class="row">
      <div class="col-md-12">
        <h1>Create New post</h1>

        <hr>
        
        <form action="{{ route('admin.create') }}" method="post">
          <div class="form-group">
            <label for="title">Title</label>

            <input type="text" class="form-control" id="title" name="title">
          </div>

          <div class="form-group">
            <label for="content">Content</label>

            <textarea class="form-control" id="content" name="content"></textarea>
          </div>

          {{ csrf_field() }}

          <button type="submit" class="btn btn-primary">
            <span>Submit</span>
          </button>
        </form>
      </div>
    </div>
  </section>
@endsection