@extends('layouts.master')

@section('content')
    <section class="post">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>About</h1>
          </div>
        
          <div class="col-sm-12">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda commodi illum nobis nostrum numquam officiis possimus provident rem repellat sint? Dicta eligendi eum hic, labore nisi non quidem quos voluptates. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa debitis magni modi nihil numquam odit pariatur recusandae repellendus sint voluptates. Blanditiis expedita minus nostrum numquam placeat porro praesentium quae soluta!</p>
          </div>
        </div>
      </div>
    </section>
@endsection