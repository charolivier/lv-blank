@extends('layouts.master')

@section('content')
  <section class="cards">
    <div class="container">
      <div class="row">
          <div class="@if (count($posts)>4) card-columns @else card-deck @endif">
            @foreach($posts as $post)
              <div class="card">
                <img class="card-img-top" src="">

                <div class="card-body">
                  <h4 class="card-title">{{ $post->title }}</h4>
                  <small class="card-subtitle">{{ $post->created_at }}</small>

                  <p class="card-text">{{ \Illuminate\Support\Str::words($post->content, 25, '...') }}</p>
                </div>

                <div class="card-footer">
                  <a class="btn btn-primary" href="{{ route('blog.post', ['id' => $post->id]) }}">Read more</a>
                </div>
              </div>
            @endforeach
          </div>
      </div>
    </div>
  </section>
@endsection