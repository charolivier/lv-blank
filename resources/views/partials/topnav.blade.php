<section class="topnav">
  <nav class="navbar navbar-expand-md navbar-wrp py-2">
    <div class="container">
      <a class="navbar-brand" href="{{ route('blog.index') }}">
				<span>Blank</span>
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('blog.index') }}">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('other.about') }}">About</a>
          </li>
        </ul>

        <div class="navbar-wrp-utilities">
          <form id="navbar-search-form" class="form-inline search-form">
            <input class="form-control" type="text" placeholder="Search" name="s">

            <button class="btn btn-primary btn-navbar-search" type="submit">
              <i class="fa fa-search"></i>
            </button>
          </form>

          <a class="btn btn-primary btn-navbar-account" href="{{ route('admin.index') }}">
            <i class="fa fa-user"></i>
          </a>					
        </div>
      </div>
    </div>
  </nav>
</section>
