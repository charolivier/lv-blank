<?php

namespace App;

// Extends the Eloquent model
// Used to access the DB
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
		// In order to make sure that we can add title and content in the Post model in one step
		// we add thst property in the Post model
		// $fillable is a protected name Laravel will recognize and it's an array
		// one string refer to one DB field we want to make mass assignable
		
    protected $fillable = ['title', 'content'];
}