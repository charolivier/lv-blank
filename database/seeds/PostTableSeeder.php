<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			$post = new \App\Post([
				"title" => "Hello World",
				"content" => "This is dummy content to get you started."
			]);

			$post->save();
    }
}
